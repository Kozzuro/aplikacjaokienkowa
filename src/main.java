import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class main {

	private JFrame frame;
	private JTextField tfLiczbaA;
	private JTextField tfLiczbaB;
	double a,b;
	String liczbaA;
	String liczbaB;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	DecimalFormat df = new DecimalFormat("###.####");
	
	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 421, 160);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnDodaj = new JButton("Dodaj");
		btnDodaj.setBounds(10, 11, 89, 23);
		frame.getContentPane().add(btnDodaj);
		
		JButton btnOdejmij = new JButton("Odejmij");
		btnOdejmij.setBounds(109, 11, 89, 23);
		frame.getContentPane().add(btnOdejmij);
		
		JButton btnPomnoz = new JButton("Pomn\u00F3\u017C");
		btnPomnoz.setBounds(208, 11, 89, 23);
		frame.getContentPane().add(btnPomnoz);
		
		JButton btnPodziel = new JButton("Podziel");
		btnPodziel.setBounds(307, 11, 89, 23);
		frame.getContentPane().add(btnPodziel);
		
		JLabel lblLiczbaB = new JLabel("Liczba B");
		lblLiczbaB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLiczbaB.setBounds(10, 70, 89, 14);
		frame.getContentPane().add(lblLiczbaB);
		
		JLabel lblLiczbaA = new JLabel("Liczba A");
		lblLiczbaA.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLiczbaA.setBounds(10, 45, 89, 14);
		frame.getContentPane().add(lblLiczbaA);
		
		tfLiczbaA = new JTextField();
		tfLiczbaA.setBounds(80, 44, 86, 20);
		frame.getContentPane().add(tfLiczbaA);
		tfLiczbaA.setColumns(10);
		
		tfLiczbaB = new JTextField();
		tfLiczbaB.setBounds(80, 69, 86, 20);
		frame.getContentPane().add(tfLiczbaB);
		tfLiczbaB.setColumns(10);
		
		JLabel lblWynik = new JLabel("Wynik: ");
		lblWynik.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblWynik.setBounds(10, 95, 386, 14);
		frame.getContentPane().add(lblWynik);
		
		JLabel lblMiniKalkulator = new JLabel("Mini Kalkulator");
		lblMiniKalkulator.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMiniKalkulator.setBounds(259, 45, 104, 39);
		frame.getContentPane().add(lblMiniKalkulator);
		
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dodawanie dodawanie  = new dodawanie();
				
				liczbaA = tfLiczbaA.getText();
				a = Double.parseDouble(liczbaA);
				
				liczbaB = tfLiczbaB.getText();
				b = Double.parseDouble(liczbaB);
				
				dodawanie.solve(a, b);

				lblWynik.setText("Wynik to: " +  String.valueOf(df.format(dodawanie.c)));
					
			}
		});
		
		btnOdejmij.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				odejmowanie odejmowanie  = new odejmowanie();
				
				liczbaA = tfLiczbaA.getText();
				a = Double.parseDouble(liczbaA);
				
				liczbaB = tfLiczbaB.getText();
				b = Double.parseDouble(liczbaB);
				
				odejmowanie.solve(a, b);

				lblWynik.setText("Wynik to: " +  String.valueOf(df.format(odejmowanie.c)));
				
			}
		});
		
		btnPomnoz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				mnozenie mnozenie  = new mnozenie();
				
				liczbaA = tfLiczbaA.getText();
				a = Double.parseDouble(liczbaA);
				
				liczbaB = tfLiczbaB.getText();
				b = Double.parseDouble(liczbaB);
				
				mnozenie.solve(a, b);

				lblWynik.setText("Wynik to: " +  String.valueOf(df.format(mnozenie.c)));
				
			}
		});
		
		btnPodziel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dzielenie dzielenie  = new dzielenie();
				
				liczbaA = tfLiczbaA.getText();
				a = Double.parseDouble(liczbaA);
				
				liczbaB = tfLiczbaB.getText();
				b = Double.parseDouble(liczbaB);
				
				dzielenie.solve(a, b);

				lblWynik.setText("Wynik to: " +  String.valueOf(df.format(dzielenie.c)));
				
			}
		});
		
		
	}
}
